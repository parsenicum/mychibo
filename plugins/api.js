import Vue from 'vue'
import axios from 'axios'

export class API {

  constructor(
    api
  ) {
    this.api = api
    this.path = 'https://src.moodboard.space/'
  }

  async coords(params){
    const { data } = await this.api.get('/coords', {
      params
    })
    return data.slice( 0, 10 )
  }

  async ean(ean){
    const { data } = await this.api.post('/list/ean', {ean} )
    return data
  }

  async fridge(name = 'test'){
    const { data } = await this.api.get('/fridge/'+name)
    return data
  }
  async fridgeadd(ean, name = 'test'){
    const { data } = await this.api.get('/fridge/'+name + '/add/'+ ean)
    return data
  }
  async fridgeremove(id, name = 'test'){
    const { data } = await this.api.get('/fridge/'+name + '/remove/'+ id)
    return data
  }

  async random( params ){
    const { data } = await this.api.get('/random', { params } )
    return data
  }


  async findmeal( meal = 'breakfast', name = 'test' ){
    const { data } = await this.api.get(`/brain/${name}/find/${meal}`)
    return data && data.length && data[ ~~(Math.random()*data.length)]
  }

  async match( params = {}, name = 'test' ){
    const { data } = await this.api.get(`/brain/${name}/match`, { params })
    return data
  }

  async train(body, name = 'test'){
    const { data } = await this.api.post(`/brain/${name}/train`, body)
    return data
  }

  async like(input, name = 'test'){
    const { data } = await this.api.post(`/brain/${name}/like`, { input })
    return data
  }

  async hate(input, name = 'test'){
    const { data } = await this.api.post(`/brain/${name}/hate`, { input })
    return data
  }

  async run(input, name = 'test' ){
    const { data } = await this.api.post(`/brain/${name}/product`, { input })
    return data
  }

}

class Connect {
  install(V, options) {
    V.prototype.$api = new API(
      axios.create({ // books server
        baseURL: this.baseURL('api')
      })
    )

  }

  baseURL(server){
    if ( process.browser ) {
      const { hostname , protocol } = window.location

      const port = {
        'api': 5000,
      }

      if( hostname === 'localhost')
        return `http://${hostname}:${port[server]}`
      else
        return `${protocol}//junctionapi.outweb.space/`
    }
  }

}

Vue.use(
  new Connect()
)
