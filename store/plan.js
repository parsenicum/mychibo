export const state = () => ({
  currentDay: 0,
  breakfast: null,
  lunch: null,
  dinner: null,
  breakfastLock: false,
  lunchLock: false,
  dinnerLock: false,
  haves: [],
  days: [
    {
      breakfast: {},
      lunch: {},
      dinner: {}
    }
  ]
})


export const getters = {
  ean({breakfast, lunch, dinner}){
    let ar = []
    if ( breakfast && breakfast.from )
      ar = [...ar, ...breakfast.from ]
      if ( lunch && lunch.from )
        ar = [...ar, ...lunch.from ]
        if ( dinner && dinner.from )
          ar = [...ar, ...dinner.from ]

    return ar.map(({Ean}) => Ean ).filter(Boolean)
  },
  today( state ){
    if ( !state.days[ state.currentDay ] )
      return {
        breakfast: {},
        lunch: {},
        dinner: {}
      }
    else
      return state.days[ state.currentDay ]
  }
}

export const mutations = {
  selectDay( state , day ){
    state.currentDay = parseInt( day )
  },
  pushBreakfast(state, item){
    state.breakfast = item
  },
  pushLunch(state, item){
    state.lunch = item
  },
  pushDinner(state, item){
    state.dinner = item
  },
  have( state, ean ){
    //state.haves.push(ean)
  },
  lock( state, { what, bool }){
    switch (what) {
      case 'dinner':
        state.dinnerLock = bool
        break;
        case 'breakfast':
          state.breakfastLock = bool
          break;
          case 'lunch':
            state.lunchLock = bool
            break;
    }
  }
}
