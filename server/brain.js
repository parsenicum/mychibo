const express = require('express')
const router = express.Router()
const brain = require('brain.js')
const www = {}

module.exports = function( recipes ){
  const config = {
    binaryThresh: 0.5,
    hiddenLayers: [3], // array of ints for the sizes of the hidden layers in the network
    activation: 'sigmoid', // supported activation types: ['sigmoid', 'relu', 'leaky-relu', 'tanh'],
    leakyReluAlpha: 0.01, // supported for activation type 'leaky-relu'
  }
  // new brain.recurrent.LSTM()

  router.use(function timeLog(req, res, next) {
    console.log( 'Time: ', Date.now() )
    next()
  })


  function getList( value ) {
    return {
      breakfast: value,
      lunch: value,
      dinner: value
    }
  }

  router.get('/ping', (_,res) => res.json({pong:true}) )

  router.post('/:netid/hate', async ({ body, params }, res) =>  {
    const { input } = body
    if (!input) return res.json({error: "no input"})

    if ( !www[params.netid] )
      www[params.netid] = new brain.NeuralNetwork( config )

    www[params.netid].train( [{ input, output: getList(0) }] )
    res.json(body)
  })

  router.post('/:netid/like', async ({body, params }, res) => {
    const { input } = body
    if (!input) return res.json({error: "no input"})

    if ( !www[params.netid] )
      www[params.netid] = new brain.NeuralNetwork( config )

    www[params.netid].train( [{ input, output: getList(1) }] )
    res.json(body)
  })

  router.post('/:netid/train', async ({body, params }, res) => {
    const { input, output } = body
    if (!input || !output) return res.json({error: "no input or output"})

    if ( !www[params.netid] )
      www[params.netid] = new brain.NeuralNetwork( config )

    www[params.netid].train( [{ input, output }] )
    res.json(body)
  })

  router.post('/:netid/product', async ({body, params }, res) => {
    const { input } = body
    if (!input) return res.json({error: "no input"})

    if ( !www[params.netid] ) return res.status(404).json({error: "network not found"})
    res.json(
      www[params.netid].run( input )
    )
  })

  router.get('/:netid/find/:meal', async ({params, query}, res) => {
    if ( !www[params.netid] )
      return res.status(404).json( {error: "network not found"} )

    if ( !['breakfast', 'lunch', 'dinner'].includes(params.meal) )
      return res.status(400).json({error: "bad request"})

    const lean = recipes.map( ( { input }, id ) => (
      { output: www[params.netid].run( input ), id })
    )

    lean.sort((aa,bb) => {
      const a = aa.output
      const b = bb.output
      a[params.meal] > b[params.meal] ? 1 : -1
    }).slice( 0, 25 )

    res.json( lean.map( ({id}) => recipes[id] ) )
  })

  router.get('/:netid/match', async ({params, query },res) => {
    const { ex } = query

    if ( !www[params.netid] ) return res.status(404).json( {error: "network not found"} )
    const lean = recipes.map( ( { input }, id ) => (
      { output: www[params.netid].run( input ), id })
    )

    lean.sort((aa,bb) => {
      const a = aa.output
      const b = bb.output
      a.breakfast + a.lunch + a.dinner > b.breakfast + b.lunch + b.dinner ? 1 : -1
    })

    /*
      сравниваем в модельке саймый лайк и самый хэйт,
      показываем тот который ближе к середине,
      юзер переоценивает его мы проверяем результаты опять
    */

    let shate = Object.keys(lean[0].output).reduce((ac,key) => ac+lean[0].output[key], 0 )
    let slike = Object.keys(lean[lean.length -1].output).reduce((ac,key) => ac+lean[lean.length -1].output[key], 0 )
    let index = shate < 1 - slike ? 0 : lean.length -1

    const add = index === 0

    if ( Array.isArray(ex) ){
      while ( ex.map(e => parseInt(e)).includes(index) ) {
        if ( add )
          index++
        else
          index--
      }
    }

    res.json({
       ...recipes[ lean[index].id ], seed: lean[index].id
    })
  })

  return router
}
