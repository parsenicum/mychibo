module.exports = function (tags) {

  function near( x, y, lat, lon ){
    const { sqrt , pow, abs } = Math
    return sqrt( pow( abs(x - lat), 2) + pow( abs( y - lon ) , 2) )
  }

  function toNormalData(d){
    const { Name , TimeRange, PictureUrls, Url, Categories, Description , Ingredients } = d
    console.log(
      d
    )
    const res = {}
    const { MinTime , MaxTime } = TimeRange || {}
    const list = Categories || []
    const subs = list.map( ({SubId}) => SubId )
    const from = Ingredients
      .reduce(
        (acc, { SubSectionIngredients }) => ([...acc,
          ...(
            SubSectionIngredients || []
          ).reduce( (ac, it) =>
            ([...ac, ...it ])
          , [] )
        ]) , []
      )

    res.title = Name
    res.desc = Description
    res.from = from
    res.input = new Array(tags.length).fill(0).map( (_, i) => subs.includes( tags[i] ) * 1 )
    res.tags = list.map(
      ({ SubName }) => SubName
    )
    res.time = TimeRange && TimeRange.MinTime
    res.image = PictureUrls && PictureUrls[0] && PictureUrls[0].Original
    res.source = Url

    return res
  }

  return {
    near, toNormalData
  }
}
