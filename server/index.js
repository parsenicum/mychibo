const express = require( 'express' )
const cors = require( 'cors' )
const fs = require('fs')
const io = require( 'socket.io' )
const Elastic = require( './elastic' )
const bodyParser = require('body-parser')


const coords = JSON.parse( fs.readFileSync(__dirname+'/data/coords.json', 'utf8') )
const recipes = JSON.parse( fs.readFileSync(__dirname+'/data/recipes.json', 'utf8') )
const tags = JSON.parse( fs.readFileSync(__dirname+'/data/tags.json', 'utf8') )

const { near, toNormalData } = require('./pure')( tags )

const elastic = new Elastic(
  'https://lastic.outweb.space',
  '7.2'
)

// .filter(
//   ( item ) => item && item.Categories && item.Categories.length
// )

/*
  сначала 10 рандомных рецептов
  обучаем по ним нейронку
  фильтруем все рецепты
  показываем их
  при свайпе переобучаем нейронку
    если рецепт не закреплен и у него рейтинг >.2 он заменяется на другой

  со всех рецептов держим список игридиентов
  при заходе даем им имена (k market ip) матчим все продукты и хардкодим api ean/price


*/

const app = express()

app.use( cors() )
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/brain', require('./brain')(
  recipes.map( r=> toNormalData(r) )
))

app.post('/list/ean', async ({body}, res) => {
  const { docs } = await elastic.get(
    body.ean //["2000797700006", "6410405054777"]
    , 'products' )
  res.json(
    docs.map(({_source}) => _source )
  )
})

app.get('/coords', ({query}, res) => {
  const { lat , lon } = query
  res.json( [...coords].sort((a,b) =>
    near(a.lat, a.lon, lat, lon ) > near(b.lat, b.lon, lat, lon ) ? 1 : -1
  ))
})

app.get('/ean', (req,res) => {
  let ar = []
  for ( const r of recipes) {
    ar = [ ...ar,
      ...toNormalData(r)
      .from.map( ({Ean}) => Ean ).filter(Boolean) ]
  }
  res.json(
    [...new Set(
      ar
    )]
  )
})

app.get('/test', (req, res) => {
  let max = 0
  let items = []
  let no =0
  for ( let i=0; i<2500;i++) {
    max = Math.max( recipes[i] && recipes[i].Ingredients && recipes[i].Ingredients.length || 0 , max )

    if ( !recipes[i].Categories ) no++

    items = [
      ...items,
      ...(recipes[i].Categories||[]).map( ({MainId, SubId , SubName }) => SubId )
    ]
    // items = [
    //     ...items,
    //     ...recipes[i].Ingredients
    //     .reduce((acc,{SubSectionIngredients}) => ([ ...acc, ...SubSectionIngredients.map(([b]) => b) ]),[] )
    //     .map(({Ean, IngredientType }) => IngredientType )
    //   ]
  }
  // res.json(
  //   [...new Set(items)]
  // )
  // res.json(
  //   max
  //    //.map(({SubSectionIngredients}) => SubSectionIngredients )
  // )
  //let [d] = recipes
  return res.json(toNormalData(recipes[ ~~( Math.random() * recipes.length ) ]))
})

app.get('/kesko/receipt', ( req, res) => {
  let [d] = recipes
  res.json(d)
})

// fridge
const fridge = new Map()

app.get('/fridge/:name', async ( {params}, res) => {
  res.json({
    products: fridge.get(params.name) || []
  })
})

app.get('/fridge/:name/add/:ean', async ( {params}, res) => {
  const { docs } = await elastic.get( [params.ean] , 'products' )
  fridge.set( params.name,
    [...(fridge.get(params.name)||[]), docs.map(({_source}) => _source ).find(t=>t) ]
  )

  res.json({
    status: "OK"
  })
})

app.get('/fridge/:name/remove/:id', async ( req, res) => {
  const { name, id } = req.params

  if ( fridge.get(name) && fridge.get(name).length > parseInt(id) )
    fridge.get(name).splice( parseInt(id), 1 )

  res.json({
    status: "OK"
  })
})

// list

app.get('/list', async ( req, res) => {
  res.json({
    products: []
  })
})

// tofridge

app.get('/list/tofridge/:id', async ( req, res) => {
  res.json({
    products: []
  })
})

app.get('/random', async ({ query }, res) => {
    const { ex } = query
    let rand = new Array( recipes.length ).fill(0).map( (_,i) => i )

    if ( Array.isArray(ex) )
      ex.forEach( item => {
        if ( rand.indexOf(item) !== -1 )
          rand.splice( rand.indexOf(item), 1 )
      })

    const seed = rand[ ~~( Math.random() * rand.length ) ]

    res.json(
      { ...toNormalData( recipes[
        seed
      ] ), seed }
    )
    // try {
    //   const { hits } = await elastic.random( Date.now().toString() )
    //   const { _source , _id } = hits.hits[0]
    //   res.json({
    //     ..._source,
    //     id: _id
    //   })
    // } catch ( e ) {
    //   console.log(e)
    //
    //   res.json({
    //     error: true
    //   })
    // }
})

app.listen(5000 , () =>
  console.log( 'Example app listening on port 5000!' )
)
