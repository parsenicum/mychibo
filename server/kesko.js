const axios = require('axios')
const fs = require('fs')

const key = 'd8df5786537c4920be0d186a534ba10d'

const api = axios.create({
  baseURL: "https://kesko.azure-api.net/",
  headers: {
    "Ocp-Apim-Subscription-Key": key
  }
})

const getIng = async ( id ) => {
  const { data } = await api.get('/ingredients/ingredients/'+id)
  console.log(
    data
  )
}

const getPriceInStore = async ( id, storeId ) => {
  const { data } = await api.get(`/products/${storeId}/${ean}`)
  console.log(
    data
  )
  return data
}

const avaliabe = async ( ean ) => {
  const { data } = await api.get('/v2/products', {
    params: {
      ean
    }
  })
  console.log(
    data[0].stores.length
  )
  return data
}

avaliabe( [6410405091857] )

// getIng(6520)


// const c = JSON.parse( fs.readFileSync( './shops.json', 'utf8' ) )
// .map(({Coordinate}) => ({ lon: Coordinate.Longitude, lat: Coordinate.Latitude }) )
//
// fs.writeFileSync('./coords.json', JSON.stringify(c), 'utf8')

// найти ближайшие магазины
// https://kesko.portal.azure-api.net/docs/services/search/operations/post-search-stores?

const findStores = async ( offset = 0 ) => {
  try {
    const { data } = await api.post('/v1/search/stores', {
      "query": "*",
      // sortOrders: {
      //   "name": "distance",
      //   "location": {
      //     "lon": 42,     /* a Number */
      //     "lat": 24      /* a Number */
      //   }
      // },
      // locationDistance: {
      //   location: {
      //     lon,     /* a Number */
      //     lat   /* a Number */
      //   },
      //   distance : km   /* a Number describing max. kilometers from location */
      // },
      "view": {
        offset
      }
    })
    return data.results

  } catch ( { response } ){
    console.log(
      response.data, response.status
    )
    return []
  }
}


const getR = async ( offset = 0 ) => {
  try {
    const { data } = await api.post('/v1/search/recipes', {
        "query": "*",
        "view": {
          offset,
          limit: 0
          // showFacets: {
          //   "facets": [
          //     "*"
          //   ],
          //   "limit": 250
          // }
        }
      }
    )
    return data.results

  } catch (e) {
    console.log(
      e
    )
    return []
  } finally {

  }
}

const f = async ( ) => {
  let ar = []
  for ( let i = 100; i<10000; i++ ) {
    const r = await findStores(
      i*100
    )
    ar = [ ...ar, ...r ]
    fs.writeFileSync( './out-2.json', JSON.stringify(ar), 'utf8' )
    console.log(
      i
    )
  }
}




const main = async () => {
  try {
    const { data } = await api.get('/ingredients/ingredients?queryTerm=')
    console.log(
      data
    )
  } catch ( e ){
    console.log(
      e
    )
  }
  // const { data } = await api.post('/files/receipts')
  // console.log(
  //   data
  // );
}


//main()
