const elasticsearch = require( 'elasticsearch' )

module.exports = class Elastic {
  constructor(
    host, apiVersion
  ){
    this.api = new elasticsearch.Client({
      host, apiVersion
    })

    this.api.ping({
      requestTimeout: 5000
    }, error => {
      if (error)
        console.trace( 'elasticsearch cluster is down!' )
      else
        console.log( 'All is well' )

    })

    // this.checkIndices().then( () =>
    //   this.putIndex().then( status =>
    //     console.log( status )
    //   ).catch( err =>
    //     console.log( err )
    //   )
    // )

  }

  async random( seed ){
    return this.api.search({
      size: 1,
      restTotalHitsAsInt: true,
      index: 'menu',
      body: {
        query: {
            function_score: {
               functions: [
                  {
                     random_score: {
                        seed
                     }
                  }
               ]
            }
         }
      }
    })
  }

  async putIndex(){
     return this.api.indices.putMapping({
        index: 'books',
        type: 'all',
        include_type_name: true,
        body: {
        properties: {
        //  tags?: string[];
        //  collection?: string; // old collection name
        //  reports?: string[]; // users reports
            tags: { type: 'text' },
            collection: { type: 'text' },
            reports: { type: 'text' },
            title: { type: 'text' },
            description: { type: 'text' },
            author: { type: 'text' },
            publisher: { type: 'text' },
            category: { type: 'text' },
            url: { type: 'text' },
            cover: { type: 'text' },
            lang: { type: 'text' },
            extension: { type: 'text' },
            filesize: { type: 'integer' },
            year: { type: 'integer' },
            month: { type: 'integer' },
            block: { type: 'boolean' },
            updated_at: { type: 'date' }
          }
        }
    })
    // , (err,resp, status) => {
    //     if (err) {
    //       console.error(err, status);
    //     }
    //     else {
    //         console.log('Successfully Created Index', status, resp);
    //     }
    // })
  }

  async checkIndices( index = { index: 'books' } ) {
    let bool = await this.api.indices.exists( index )
    if ( !bool ) await this.api.indices.create( index )

    return true
  }

  async bulk( body ){
    const result = await this.api.bulk({
      refresh: true,
      index: 'books',
      type: 'all',
      body
    })

    return result
  }

  async count(){
    const { count } = await this.api.count({
      index: 'books',
      type: 'all',
      // body: {
      //   query: {
      //     filtered: {
      //       filter: {
      //         terms: {
      //           foo: ['bar']
      //         }
      //       }
      //     }
      //   }
      // }
    })

    return count
  }

  async search( query , pagination ){
    const res = await this.api.search({
      //defaultOperator: "AND", //"OR",
      from: pagination.from || 0,
      size: pagination.size || 10,
      restTotalHitsAsInt: true,
      index: 'books',
      type: 'all',
      body: {
        query: {
          multi_match: {
            query,
            fields: [ 'title', 'description', 'author', 'publisher' ]
          }
          // match: {
          //   title,
          // }
        },
        // facets: {
        //   tags: {
        //     terms: {
        //       block: false
        //     }
        //   }
        // }
      }
    })

    return res
  }

  async get( id , index = 'books'){
    let res

    if ( Array.isArray(id) )
      res = await this.api.mget({
          index,
          body: {
            ids: id
          }
        })

    else
       res = await this.api.get({
          index,
          id
        })

    return res
  }

  async remove( id ){
    await this.api.delete({
      index: 'books',
      type: 'all',
      id
    })
  }

  // async create( id: string, book: Book ) {
  //   const res = await this.api.index({
  //     refresh: true,
  //     index: 'books',
  //     type: 'all',
  //     id,
  //     body: book
  //   })
  //
  //   return res
  // }
  //
  // async update( id : string, doc: any ){
  //   const res = await this.api.update({
  //     index: 'books',
  //     type: 'all',
  //     id,
  //     body: {
  //       doc
  //     }
  //   })
  //
  //   return res
  // }
}
