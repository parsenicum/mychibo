export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: require( './head.json' ),

  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/api',
    '@/plugins/touch'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    'nuxt-leaflet',
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
