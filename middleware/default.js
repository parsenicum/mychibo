export default function({ store, redirect, route }) {
  if ( route.query.user ) {
    store.commit('user/set', route.query.user )
  }
  
  if (!store.state.user.name)
    return redirect('/login')
}
